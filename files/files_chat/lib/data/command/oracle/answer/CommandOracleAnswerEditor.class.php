<?php
namespace chat\data\command\oracle\answer;
use chat\system\cache\builder\OracleAnswerCacheBuilder;
use wcf\data\DatabaseObjectEditor;
use wcf\data\IEditableCachedObject;
use wcf\system\io\AtomicWriter;
use wcf\system\Regex;

/**
 * Provides functions to edit an oracle answer
 *
 * @author		Daniel (Keito) Lang
 * @copyright	2014-2023 da-lang.net
 * @license		da-lang.net Commercial License <https://da-lang.net/license/commercial.txt>
 * @package		net.dalang.chat.command.oracle
 */
class CommandOracleAnswerEditor extends DatabaseObjectEditor implements IEditableCachedObject {
	/**
	 * @inheritdoc
	 */
	protected static $baseClass = CommandOracleAnswer::class;
	
	/**
	 * @inheritdoc
	 */
	public static function resetCache() {
		OracleAnswerCacheBuilder::getInstance()->reset();
	}
	
	/**
	 * generates a TPL file with all answers that require i18n registration in advance
	 * rememberli:	this is necessary because i18n-answers are displayed in the language of the user who issued
	 * 				the command, rather than in the language of the 'reading' user.
	 */
	public static function buildLanguageFile() {
		$cachedAnswers = CommandOracleAnswerCache::getInstance()->getAllAnswers();
		
		$file = new AtomicWriter(CHAT_DIR.'templates/oracleCommandLanguage.tpl');
		$file->write("Language.addObject({ 'chat.messageType.net.dalang.chat.messageType.oracle': '{lang __literal=true}chat.messageType.net.dalang.chat.messageType.oracle{/lang}' })\n");
		
		$regex = new Regex('^([a-zA-Z0-9-_]+\.)+[a-zA-Z0-9-_]+$');
		foreach ($cachedAnswers as $cachedAnswer) {
			if ($regex->match($cachedAnswer->answer)) {
				$file->write(sprintf(
					"{jsphrase name='".$cachedAnswer->answer."'}\n"
				));
			}
		}
		
		$file->flush();
		$file->close();
	}
}