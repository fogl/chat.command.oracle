<?php
namespace chat\data\command\oracle\answer;
use chat\system\cache\builder\OracleAnswerCacheBuilder;
use wcf\system\SingletonFactory;

/**
 * Manages the oracle answer cache
 *
 * @author		Daniel (Keito) Lang
 * @copyright	2014-2023 da-lang.net
 * @license		da-lang.net Commercial License <https://da-lang.net/license/commercial.txt>
 * @package		net.dalang.chat.command.oracle
 */
class CommandOracleAnswerCache extends SingletonFactory {
	/**
	 * cached answers
	 * @var CommandOracleAnswer[]
	 */
	protected $cachedAnswers;
	
	/**
	 * @inheritdoc
	 */
	protected function init() {
		$this->cachedAnswers = OracleAnswerCacheBuilder::getInstance()->getData();
	}
	
	/**
	 * Returns all cached answers
	 * @return	CommandOracleAnswer[]
	 */
	public function getAllAnswers():array {
		return $this->cachedAnswers;
	}
	
	/**
	 * Returns a specific answer by id
	 * @param	integer		$answerID
	 * @return	CommandOracleAnswer|null
	 */
	public function getAnswer(int $answerID = 0):?CommandOracleAnswer {
		return ($this->cachedAnswers[$answerID] ?? null);
	}
}