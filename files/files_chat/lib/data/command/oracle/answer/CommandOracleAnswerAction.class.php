<?php
namespace chat\data\command\oracle\answer;
use wcf\data\AbstractDatabaseObjectAction;

/**
 * Executes oracle answer related actions
 *
 * @author		Daniel (Keito) Lang
 * @copyright	2014-2023 da-lang.net
 * @license		da-lang.net Commercial License <https://da-lang.net/license/commercial.txt>
 * @package		net.dalang.chat.command.oracle
 */
class CommandOracleAnswerAction extends AbstractDatabaseObjectAction {
	/**
	 * @inheritdoc
	 */
	protected $className = CommandOracleAnswerEditor::class;
	
	/**
	 * @inheritdoc
	 */
	protected $permissionsCreate = ['admin.chat.canManageOracle'];
	protected $permissionsDelete = ['admin.chat.canManageOracle'];
	protected $permissionsUpdate = ['admin.chat.canManageOracle'];
}