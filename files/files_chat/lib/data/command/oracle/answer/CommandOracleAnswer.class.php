<?php
namespace chat\data\command\oracle\answer;
use wcf\data\DatabaseObject;
use wcf\system\WCF;

/**
 * Represents an oracle answer
 *
 * @author		Daniel (Keito) Lang
 * @copyright	2014-2023 da-lang.net
 * @license		da-lang.net Commercial License <https://da-lang.net/license/commercial.txt>
 * @package		net.dalang.chat.command.oracle
 *
 * @property-read 	integer		answerID
 * @property-read 	string		answer
 * @property-read 	string		color
 */
class CommandOracleAnswer extends DatabaseObject {
	/**
	 * @inheritdoc
	 */
	protected static $databaseTableIndexName = 'answerID';
	
	/**
	 * Returns the language var of this answer
	 * @return	string
	 */
	public function getAnswer(): string {
		return WCF::getLanguage()->get($this->answer);
	}
}