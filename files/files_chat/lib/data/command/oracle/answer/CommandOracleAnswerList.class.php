<?php
namespace chat\data\command\oracle\answer;
use wcf\data\DatabaseObjectList;

/**
 * Represents a list of oracle answers
 *
 * @author		Daniel (Keito) Lang
 * @copyright	2014-2023 da-lang.net
 * @license		da-lang.net Commercial License <https://da-lang.net/license/commercial.txt>
 * @package		net.dalang.chat.command.oracle
 */
class CommandOracleAnswerList extends DatabaseObjectList {
	/**
	 * @inheritdoc
	 */
	public $className = CommandOracleAnswer::class;
}