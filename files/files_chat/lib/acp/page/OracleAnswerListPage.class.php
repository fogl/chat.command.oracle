<?php
namespace chat\acp\page;
use chat\data\command\oracle\answer\CommandOracleAnswerList;
use wcf\page\SortablePage;

/**
 * Shows the oracle answer list page
 *
 * @author		Daniel (Keito) Lang
 * @copyright	2014-2023 da-lang.net
 * @license		da-lang.net Commercial License <https://da-lang.net/license/commercial.txt>
 * @package		net.dalang.chat.command.oracle
 */
class OracleAnswerListPage extends SortablePage {
	/**
	 * @inheritdoc
	 */
	public $activeMenuItem = 'chat.acp.menu.link.command.oracle.answer.list';
	
	/**
	 * @inheritdoc
	 */
	public $neededPermissions = ['admin.chat.canManageOracle'];
	
	/**
	 * @inheritdoc
	 */
	public $objectListClassName = CommandOracleAnswerList::class;
	
	/**
	 * @inheritdoc
	 */
	public $validSortFields = ['answerID', 'answer'];
	
	/**
	 * @inheritdoc
	 */
	public $defaultSortField = 'answer';
	
	/**
	 * @inheritdoc
	 */
	public $defaultSortOrder = 'ASC';
}