<?php
namespace chat\acp\form;
use chat\data\command\oracle\answer\CommandOracleAnswerAction;
use chat\data\command\oracle\answer\CommandOracleAnswerEditor;
use wcf\data\package\PackageCache;
use wcf\form\AbstractForm;
use wcf\system\exception\UserInputException;
use wcf\system\language\I18nHandler;
use wcf\system\request\LinkHandler;
use wcf\system\WCF;
use wcf\util\StringUtil;

/**
 * Shows the oracle answer add form
 *
 * @author		Daniel (Keito) Lang
 * @copyright	2014-2023 da-lang.net
 * @license		da-lang.net Commercial License <https://da-lang.net/license/commercial.txt>
 * @package		net.dalang.chat.command.oracle
 */
class OracleAnswerAddForm extends AbstractForm {
	/**
	 * @inheritdoc
	 */
	public $activeMenuItem = 'chat.acp.menu.link.command.oracle.answer.add';
	
	/**
	 * @inheritdoc
	 */
	public $neededPermissions = ['admin.chat.canManageOracle'];
	
	/**
	 * answer
	 * @var string
	 */
	public $answer = '';
	
	/**
	 * color
	 * @var string
	 */
	public $color = '';
	
	/**
	 * customColor
	 * @var string
	 */
	public $customColor = '';
	
	/**
	 * availableColors
	 * @var string[]
	 */
	public $availableColors = [
		'yellow',
		'orange',
		'brown',
		'red',
		'pink',
		'purple',
		'blue',
		'green',
		'black',
		'' // no color
	];
	
	/**
	 * @inheritdoc
	 */
	public function readParameters() {
		parent::readParameters();
		
		I18nHandler::getInstance()->register('answer');
	}
	
	/**
	 * @inheritdoc
	 */
	public function readFormParameters() {
		parent::readFormParameters();
		
		I18nHandler::getInstance()->readValues();
		
		if (I18nHandler::getInstance()->isPlainValue('answer')) {
			$this->answer = I18nHandler::getInstance()->getValue('answer');
		}
		
		if (isset($_POST['color'])) $this->color = StringUtil::trim($_POST['color']);
		if (isset($_POST['customColor'])) $this->customColor = StringUtil::trim($_POST['customColor']);
	}
	
	/**
	 * @inheritdoc
	 */
	public function validate() {
		parent::validate();
		
		// validate answer
		if (!I18nHandler::getInstance()->validateValue('answer')) {
			if (I18nHandler::getInstance()->isPlainValue('answer')) {
				throw new UserInputException('answer');
			}
			else {
				throw new UserInputException('answer', 'multilingual');
			}
		}
		
		// validate color
		if (!in_array($this->color, $this->availableColors)) {
			$this->color = $this->customColor;
		}
	}
	
	/**
	 * @inheritdoc
	 */
	public function save() {
		parent::save();
		
		$this->objectAction = new CommandOracleAnswerAction([], 'create', [
			'data' => [
				'answer' => $this->answer,
				'color' => $this->color
			]
		]);
		$resultValues = $this->objectAction->executeAction()['returnValues'];
		$data = [];
		
		if (!I18nHandler::getInstance()->isPlainValue('answer')) {
			I18nHandler::getInstance()->save('answer', 'chat.messageType.net.dalang.chat.messageType.oracle.answer'.$resultValues->answerID, 'chat.messageType', PackageCache::getInstance()->getPackageID('be.bastelstu.chat'));
			$data['answer'] = 'chat.messageType.net.dalang.chat.messageType.oracle.answer'.$resultValues->answerID;
		}
		
		$answerEditor = new CommandOracleAnswerEditor($resultValues);
		$answerEditor->update($data);
		
		CommandOracleAnswerEditor::buildLanguageFile();
		
		$this->saved();
		
		WCF::getTPL()->assign([
			'success' => true,
			'objectEditLink' => LinkHandler::getInstance()->getControllerLink(OracleAnswerEditForm::class, ['id' => $resultValues->answerID])
		]);
		
		$this->answer = $this->color = $this->customColor = '';
		I18nHandler::getInstance()->reset();
	}
	
	/**
	 * @inheritdoc
	 */
	public function assignVariables() {
		parent::assignVariables();
		
		I18nHandler::getInstance()->assignVariables();
		
		WCF::getTPL()->assign([
			'action' => 'add',
			'answer' => $this->answer,
			'color' => $this->color,
			'customColor' => $this->customColor,
			'availableColors' => $this->availableColors
		]);
	}
}