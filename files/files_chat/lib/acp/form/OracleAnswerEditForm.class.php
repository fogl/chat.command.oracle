<?php
namespace chat\acp\form;
use chat\data\command\oracle\answer\CommandOracleAnswer;
use chat\data\command\oracle\answer\CommandOracleAnswerAction;
use chat\data\command\oracle\answer\CommandOracleAnswerEditor;
use wcf\data\package\PackageCache;
use wcf\form\AbstractForm;
use wcf\system\exception\IllegalLinkException;
use wcf\system\language\I18nHandler;
use wcf\system\WCF;

/**
 * Shows the oracle answer edit form
 *
 * @author		Daniel (Keito) Lang
 * @copyright	2014-2023 da-lang.net
 * @license		da-lang.net Commercial License <https://da-lang.net/license/commercial.txt>
 * @package		net.dalang.chat.command.oracle
 */
class OracleAnswerEditForm extends OracleAnswerAddForm {
	/**
	 * @inheritdoc
	 */
	public $activeMenuItem = 'chat.acp.menu.link.chat';
	
	/**
	 * answerObj
	 * @var CommandOracleAnswer
	 */
	public $answerObj;
	
	/**
	 * answerID
	 * @var integer
	 */
	public $answerID = 0;
	
	/**
	 * @inheritdoc
	 */
	public function readParameters() {
		parent::readParameters();
		
		if (isset($_REQUEST['id'])) $this->answerID = intval($_REQUEST['id']);
		$this->answerObj = new CommandOracleAnswer($this->answerID);
		if (!$this->answerObj->answerID) {
			throw new IllegalLinkException();
		}
	}
	
	/**
	 * @inheritdoc
	 */
	public function readData() {
		parent::readData();
		
		if (empty($_POST)) {
			I18nHandler::getInstance()->setOptions('answer', PackageCache::getInstance()->getPackageID('be.bastelstu.chat'), $this->answerObj->answer, 'chat.messageType.net.dalang.chat.messageType.oracle.answer\+d');
			$this->answer = $this->answerObj->answer;
			
			$this->color = $this->answerObj->color;
			if (!in_array($this->answerObj->color, $this->availableColors)) {
				$this->color = 'custom';
				$this->customColor = $this->answerObj->color;
			}
			
		}
	}
	
	/**
	 * @inheritdoc
	 */
	public function save() {
		AbstractForm::save();
		
		$this->answer = 'chat.messageType.net.dalang.chat.messageType.oracle.answer'.$this->answerObj->answerID;
		if (I18nHandler::getInstance()->isPlainValue('answer')) {
			I18nHandler::getInstance()->remove($this->answer);
			$this->answer = I18nHandler::getInstance()->getValue('answer');
		}
		else {
			I18nHandler::getInstance()->save('answer', $this->answer, 'chat.messageType', PackageCache::getInstance()->getPackageID('be.bastelstu.chat'));
		}
		
		$this->objectAction = new CommandOracleAnswerAction([$this->answerObj], 'update', [
			'data' => [
				'answer' => $this->answer,
				'color' => $this->color
			]
		]);
		$this->objectAction->executeAction();
		
		CommandOracleAnswerEditor::buildLanguageFile();
		
		$this->saved();
		
		WCF::getTPL()->assign('success', true);
	}
	
	/**
	 * @inheritdoc
	 */
	public function assignVariables() {
		parent::assignVariables();
		
		I18nHandler::getInstance()->assignVariables(!empty($_POST));
		
		WCF::getTPL()->assign([
			'action' => 'edit',
			'answerObj' => $this->answerObj,
			'answerID' => $this->answerID
		]);
	}
}