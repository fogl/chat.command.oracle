<?php
namespace chat\system\command;
use chat\data\command\oracle\answer\CommandOracleAnswerCache;
use chat\data\message\MessageAction;
use chat\data\room\Room;
use wcf\data\user\UserProfile;
use wcf\system\exception\PermissionDeniedException;
use wcf\system\exception\UserInputException;
use wcf\system\flood\FloodControl;
use wcf\system\WCF;
use wcf\util\StringUtil;

/**
 * oracle command
 *
 * @author		Daniel (Keito) Lang
 * @copyright	2014-2023 da-lang.net
 * @license		da-lang.net Commercial License <https://da-lang.net/license/commercial.txt>
 * @package		net.dalang.chat.command.oracle
 */
class OracleCommand extends AbstractCommand implements ICommand {
	/**
	 * subject
	 * @var string
	 */
	public $subject = '';
	
	/**
	 * @inheritdoc
	 */
	public function getJavaScriptModuleName() {
		return 'Dalang/Chat/Command/Oracle';
	}
	
	/**
	 * @inheritdoc
	 */
	public function validate($parameters, Room $room, UserProfile $user = null) {
		if ($user === null) $user = new UserProfile(WCF::getUser());
		
		if (!$room->canWritePublicly($user)) {
			throw new PermissionDeniedException();
		}
		
		if (!$user->getPermission('user.chat.canUseOracle')) {
			throw new PermissionDeniedException();
		}
		
		$lastCommand = FloodControl::getInstance()->getLastTime('net.dalang.chat.command.oracle');
		if ($lastCommand !== null && CHAT_ORACLE_COOLDOWN > 0) {
			$timeDiff = TIME_NOW - $lastCommand;
			if ($timeDiff <= CHAT_ORACLE_COOLDOWN) {
				throw new UserInputException('message', WCF::getLanguage()->getDynamicVariable('chat.error.oracle.cooldown', ['restTime' => CHAT_ORACLE_COOLDOWN - $timeDiff]));
			}
		}
		
		$this->subject = StringUtil::truncate(StringUtil::stripHTML($parameters['text']), 50, '', false);
		
		// validate subject usage
		if (!$user->getPermission('user.chat.canUseOracleSubject')) {
			$this->subject = '';
		}
	}
	
	/**
	 * @inheritdoc
	 */
	public function execute($parameters, Room $room, UserProfile $user = null) {
		if ($user === null) $user = new UserProfile(WCF::getUser());
		
		$answerCount = count(CommandOracleAnswerCache::getInstance()->getAllAnswers());
		if ($answerCount == 0) {
			return; // do nothing if there aren't any answers
		}
		
		FloodControl::getInstance()->registerContent('net.dalang.chat.command.oracle');
		
		// get a random answer
		$answerList = array_values(CommandOracleAnswerCache::getInstance()->getAllAnswers());
		$randAnswer = $answerList[mt_rand(0, $answerCount - 1)];
		
		$action = new MessageAction([], 'create', [
				'data' => [
						'roomID' => $room->roomID,
						'userID' => $user->userID,
						'username' => $user->username,
						'time' => TIME_NOW,
						'objectTypeID' => $this->getMessageObjectTypeID('net.dalang.chat.messageType.oracle'),
						'payload' => serialize(['subject' => $this->subject, 'color' => $randAnswer->color, 'answer' => $randAnswer->answer])
				],
				'updateTimestamp' => true,
				'grantPoints' => false
		]);
		$action->executeAction();
	}
}