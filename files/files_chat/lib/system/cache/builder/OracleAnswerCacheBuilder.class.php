<?php
namespace chat\system\cache\builder;
use chat\data\command\oracle\answer\CommandOracleAnswerList;
use wcf\system\cache\builder\AbstractCacheBuilder;

/**
 * Caches the oracle answers
 *
 * @author		Daniel (Keito) Lang
 * @copyright	2014-2023 da-lang.net
 * @license		da-lang.net Commercial License <https://da-lang.net/license/commercial.txt>
 * @package		net.dalang.chat.command.oracle
 */
class OracleAnswerCacheBuilder extends AbstractCacheBuilder {
	/**
	 * @inheritdoc
	 */
	protected function rebuild(array $parameters) {
		$answerList = new CommandOracleAnswerList();
		$answerList->readObjects();
		
		return $answerList->getObjects();
	}
}