<?php

/**
 * @author		Daniel (Keito) Lang
 * @copyright	2014-2024 da-lang.net
 * @license		da-lang.net Commercial License <https://da-lang.net/license/commercial.txt>
 * @package		net.dalang.chat.command.oracle
 */

use wcf\system\database\table\column\NotNullVarchar255DatabaseTableColumn;
use wcf\system\database\table\column\ObjectIdDatabaseTableColumn;
use wcf\system\database\table\DatabaseTable;
use wcf\system\database\table\index\DatabaseTablePrimaryIndex;

return [
	DatabaseTable::create('chat1_command_oracle_answer')
		->columns([
			ObjectIdDatabaseTableColumn::create('answerID'),
			NotNullVarchar255DatabaseTableColumn::create('answer')
				->defaultValue(''),
			NotNullVarchar255DatabaseTableColumn::create('color')
				->defaultValue('')
		])
		->indices([
			DatabaseTablePrimaryIndex::create()->columns(['answerID'])
		])
];