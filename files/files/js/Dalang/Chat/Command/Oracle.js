/**
 * oracle command
 *
 * @author		Daniel (Keito) Lang
 * @copyright	2014-2023 da-lang.net
 * @license		da-lang.net Commercial License <https://da-lang.net/license/commercial.txt>
 * @package		net.dalang.chat.command.oracle
 * @module		Dalang/Chat/Command/Oracle
 */

define(['Bastelstu.be/Chat/Command', 'Bastelstu.be/Chat/Parser'], function (Command, Parser) {
	"use strict";
	
	class Oracle extends Command {
		getParameterParser() {
			return Parser.Rest.map(text => ({ text }))
		}
	}
	
	return Oracle
});