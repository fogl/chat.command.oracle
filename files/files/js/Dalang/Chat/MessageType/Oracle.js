/**
 * oracle message type
 *
 * @author		Daniel (Keito) Lang
 * @copyright	2014-2023 da-lang.net
 * @license		da-lang.net Commercial License <https://da-lang.net/license/commercial.txt>
 * @package		net.dalang.chat.command.oracle
 * @module		Dalang/Chat/MessageType/Oracle
 */

define(['Bastelstu.be/Chat/MessageType'], function (MessageType) {
	"use strict";
	
	class Oracle extends MessageType { }
	
	return Oracle
});