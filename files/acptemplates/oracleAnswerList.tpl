{include file='header' pageTitle="chat.acp.command.oracle.answer.list"}

<header class="contentHeader">
	<div class="contentHeaderTitle">
		<h1 class="contentTitle">{lang}chat.acp.command.oracle.answer.list{/lang}</h1>
	</div>

	<nav class="contentHeaderNavigation">
		<ul>
			<li><a href="{link application='chat' controller='OracleAnswerAdd'}{/link}" class="button">{icon name='plus'} <span>{lang}chat.acp.command.oracle.answer.add{/lang}</span></a></li>
		</ul>
	</nav>
</header>

{hascontent}
	<div class="paginationTop">
        {content}
        {pages print=true assign=pageLinks application='chat' controller='OracleAnswerList' link="pageNo=%d&sortField=$sortField&sortOrder=$sortOrder"}
        {/content}
	</div>
{/hascontent}

{if $objects|count}
	<section class="section tabularBox">
		<table class="table jsObjectActionContainer" data-object-action-class-name="chat\data\command\oracle\answer\CommandOracleAnswerAction">
			<thead>
			<tr>
				<th class="columnID columnAnswerID{if $sortField == 'answerID'} active {@$sortOrder}{/if}" colspan="2"><a href="{link application='chat' controller='OracleAnswerList'}pageNo={@$pageNo}&sortField=answerID&sortOrder={if $sortField == 'answerID' && $sortOrder == 'ASC'}DESC{else}ASC{/if}{/link}">{lang}wcf.global.objectID{/lang}</a></th>
				<th class="columnText columnAnswer{if $sortField == 'answer'} active {@$sortOrder}{/if}"><a href="{link application='chat' controller='OracleAnswerList'}pageNo={@$pageNo}&sortField=answer&sortOrder={if $sortField == 'answer' && $sortOrder == 'ASC'}DESC{else}ASC{/if}{/link}">{lang}chat.acp.command.oracle.answer.answer{/lang}</a></th>
			</tr>
			</thead>
			<tbody>
            {foreach from=$objects item=answer}
				<tr class="jsAnswerRow jsObjectActionObject" data-object-id="{$answer->answerID}">
					<td class="columnIcon">
						<a href="{link application='chat' controller='OracleAnswerEdit' id=$answer->answerID}{/link}" title="{lang}wcf.global.button.edit{/lang}" class="jsTooltip">{icon name='pencil'}</a>
                        {objectAction action="delete" confirmMessage="chat.acp.command.oracle.answer.delete.confirmMessage"}
					</td>
					<td class="columnID columnAnswerID">{#$answer->answerID}</td>
					<td class="columnText columnAnswer"><span class="badge label {$answer->color}">{$answer->getAnswer()}</span></td>
				</tr>
            {/foreach}
			</tbody>
		</table>
	</section>

	<footer class="contentFooter">
        {hascontent}
			<div class="paginationBottom">
                {content}{@$pageLinks}{/content}
			</div>
        {/hascontent}

		<nav class="contentFooterNavigation">
			<ul>
				<li><a href="{link application='chat' controller='OracleAnswerAdd'}{/link}" class="button">{icon name='plus'} <span>{lang}chat.acp.command.oracle.answer.add{/lang}</span></a></li>
			</ul>
		</nav>
	</footer>
{else}
	<p class="info">{lang}wcf.global.noItems{/lang}</p>
{/if}

{include file='footer'}