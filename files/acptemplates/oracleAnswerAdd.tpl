{include file='header' pageTitle='chat.acp.command.oracle.answer.'|concat:$action}

<header class="contentHeader">
	<div class="contentHeaderTitle">
		<h1 class="contentTitle">{lang}chat.acp.command.oracle.answer.{$action}{/lang}</h1>
	</div>

	<nav class="contentHeaderNavigation">
		<ul>
			<li><a href="{link application='chat' controller='OracleAnswerList'}{/link}" class="button">{icon name='list'} <span>{lang}chat.acp.command.oracle.answer.list{/lang}</span></a></li>
		</ul>
	</nav>
</header>

{include file='formNotice'}

<form method="post" action="{if $action == 'add'}{link application='chat' controller='OracleAnswerAdd'}{/link}{else}{link application='chat' controller='OracleAnswerEdit' id=$answerID}{/link}{/if}">
	<section class="section">
		<dl{if $errorField == 'answer'} class="formError"{/if}>
			<dt><label for="answer">{lang}chat.acp.command.oracle.answer.answer{/lang}</label></dt>
			<dd>
				<input type="text" id="answer" name="answer" value="{$i18nPlainValues['answer']}" class="long" maxlength="255" minlength="1">
                {if $errorField == 'answer'}
					<small class="innerError">
                        {if $errorType == 'empty'}
                            {lang}wcf.global.form.error.empty{/lang}
                        {elseif $errorType == 'multilingual'}
                            {lang}wcf.global.form.error.multilingual{/lang}
                        {else}
                            {lang}chat.acp.command.oracle.answer.answer.error.{@$errorType}{/lang}
                        {/if}
					</small>
                {/if}
			</dd>
		</dl>
        {include file='multipleLanguageInputJavascript' elementIdentifier='answer' forceSelection=false}

		<dl>
			<dt>{lang}chat.acp.command.oracle.answer.color{/lang}</dt>
			<dd>
				<ul id="labelList" class="inlineList">
                    {foreach from=$availableColors item=_color}
						<li>
							<label><input type="radio" id="color" name="color" value="{$_color}"{if $color == $_color} checked{/if}> <span class="badge label {$_color}">{lang}chat.acp.command.oracle.answer.answer{/lang}</span></label>
						</li>
                    {/foreach}
					<li style="display: flex;">
						<input type="radio" id="color" name="color" value="custom"{if $color == 'custom'} checked{/if} style="margin-right: 7px;"> <span><input type="text" id="customColor" name="customColor" value="{$customColor}" class="long" maxlength="255"></span>
					</li>
				</ul>
				<small>{lang}chat.acp.command.oracle.answer.color.description{/lang}</small>
			</dd>
		</dl>
	</section>

	<div class="formSubmit">
		<input type="submit" value="{lang}wcf.global.button.submit{/lang}" accesskey="s">
        {csrfToken}
	</div>
</form>

{include file='footer'}