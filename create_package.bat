set pname=net.dalang.chat.command.oracle_200.tar

mkdir package

copy package.xml package
copy xml\*.xml package

mkdir package\language
copy language\*.xml package\language

tar -cf package\files.tar -C files\files *
tar -cf package\files_chat.tar -C files\files_chat *
tar -cf package\templates_chat.tar -C files\template_chat *
tar -cf package\acptemplates.tar -C files\acptemplates *

tar -cf %pname% -C package *

rmdir /S /Q package
move %pname% ..\%pname%